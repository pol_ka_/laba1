//
// Created by Полина Калугина on 05.01.2024.
//
#include "image.h"

#ifndef ASSIGNMENT_IMAGE_ROTATION2_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION2_ROTATE_H

struct image rotate(struct image const source);

#endif //ASSIGNMENT_IMAGE_ROTATION2_ROTATE_H




